=============
Addon Manager
=============

.. autoclass:: datalogger.api.addons.AddonManager
  :members: add_addon, discover_addons, load_new, run_selected
