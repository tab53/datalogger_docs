============
Introduction
============

This documentation, in its current form, is simply an API Reference for the CUED DataLogger developers.

Hopefully at some point it will develop to include a full manual and other such exciting things.
